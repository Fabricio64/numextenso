public class Extenso implements IExercicioTeste
{
    @Override
    public String formatarNumero(int numero) throws IllegalArgumentException 
    {
        if(numero <= 0 || numero > 999999)
        {
            throw new IllegalArgumentException("Número menor ou igual a zero");
        }
        else
        {
            return CasaDosMil(numero);
        }
    }
    
    
    private String UmA19(int numero)
    {
        switch (numero) 
        {
            case 1:
                return "um";

            case 2:
                return "dois";

            case 3:
                return "três";

            case 4:
                return "quatro";

            case 5:
                return "cinco";

            case 6:
                return "seis";

            case 7:
                return "sete";

            case 8:
                return "oito";

            case 9:
                return "nove";

            case 10:
                return "dez";

            case 11:
                return "onze";

            case 12:
                return "doze";

            case 13:
                return "treze";

            case 14:
                return "quatorze";

            case 15:
                return "quinze";

            case 16:
                return "dezesseis";

            case 17:
                return "dezessete";

            case 18:
                return "dezoito";

            case 19:
                return "dezenove";

            default:
                return null;
    }
}
    
    
    private String VinteACem(int numero)
    {
        final int DIVISOR = 10;
        
        if(numero <= DIVISOR * 2)
        {
            return UmA19(numero);
        }
        
        StringBuilder retorno = new StringBuilder();

        switch(numero / DIVISOR)
        {
            case 2:
                retorno.append("vinte");
                break;

            case 3:
                retorno.append("trinta");
                break;

            case 4:
                retorno.append("quarenta");
                break;

            case 5:
                retorno.append("cinquenta");
                break;

            case 6:
                retorno.append("sessenta");
                break;

            case 7:
                retorno.append("setenta");
                break;

            case 8:
                retorno.append("oitenta");
                break;

            case 9:
                retorno.append("noventa");
                break;
                
            case 10:
                retorno.append("cem");
                break;
        }
        
        if(numero % DIVISOR != 0)
        {
            retorno.append(" e ");
            retorno.append( UmA19(numero % 10) );
        }
        
        return retorno.toString();
    }
    
    
    private String CentoUmAMil(int numero)
    {      
        final int DIVISOR = 100;
        
        if(numero <= DIVISOR)
        {
            return VinteACem(numero);
        }
        
        StringBuilder retorno = new StringBuilder();
        
        switch(numero / DIVISOR)
        {
            case 1:
                retorno.append("cento");
                break;
                
            case 2:
                retorno.append("duzentos");
                break;

            case 3:
                retorno.append("trezentos");
                break;

            case 4:
                retorno.append("quatrocentos");
                break;

            case 5:
                retorno.append("quinhentos");
                break;

            case 6:
                retorno.append("seiscentos");
                break;

            case 7:
                retorno.append("setecentos");
                break;

            case 8:
                retorno.append("oitocentos");
                break;

            case 9:
                retorno.append("novecentos");
                break;
                
            case 10:
                retorno.append("mil");
                break;
        }
        
        if(numero % DIVISOR != 0)
        {
            retorno.append(" e ");
            
            retorno.append( VinteACem( numero % DIVISOR ) );
        }
        
        return retorno.toString();
    }
    
    
    private String CasaDosMil(int numero)
    {
        final int DIVISOR = 1000;
        
        if(numero <= DIVISOR)
        {
            return CentoUmAMil(numero);
        }
        
        StringBuilder retorno = new StringBuilder();
        
        if(numero / DIVISOR > 1)
        {
            retorno.append( CentoUmAMil(numero / 1000) );
            retorno.append(" ");
        }
        
        retorno.append("mil");
        
        if( numero % DIVISOR > 0 )
        {
            retorno.append(" e ");
            retorno.append( CentoUmAMil(numero % DIVISOR) );
        }
        
        return retorno.toString();
    }
}
