import org.junit.*;

import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author 20151lbsi120130
 */
public class ExtensoTest 
{
   static IExercicioTeste classeTeste;
   
   
   @BeforeClass
   public static void before() throws Exception
   {
       classeTeste = new Extenso();
   }
   
   
   @AfterClass
   public static void after() throws Exception
   {
   }
   
   
   @Test(expected = IllegalArgumentException.class)
   public void testNegativo() throws Exception
   {
       String t1 = classeTeste.formatarNumero(-40);
       
       assertEquals(t1, null);
   }
   
   
   @Test(expected = IllegalArgumentException.class)
   public void testZero() throws Exception
   {
       String t1 = classeTeste.formatarNumero(0);
       
       assertEquals(t1, null);
   }
   
   
   @Test(expected = IllegalArgumentException.class)
   public void testLimite() throws Exception
   {
       String t1 = classeTeste.formatarNumero(999000000);
       
       assertEquals(t1, null);
   }
    
    
   @Test
   public void testeVinte() throws Exception
   {
       String t1 = classeTeste.formatarNumero(3).toLowerCase();
       String t2 = classeTeste.formatarNumero(9).toLowerCase();
       String t3 = classeTeste.formatarNumero(16).toLowerCase();
       
       assertEquals("três", t1);
       assertEquals("nove", t2);
       assertEquals("dezesseis", t3);
   }
   
   
   @Test
   public void testeCem() throws Exception
   {
       String t1 = classeTeste.formatarNumero(56).toLowerCase();
       String t2 = classeTeste.formatarNumero(48).toLowerCase();
       String t3 = classeTeste.formatarNumero(90).toLowerCase();
       String t4 = classeTeste.formatarNumero(100).toLowerCase();
       
       assertEquals("cinquenta e seis", t1);
       assertEquals("quarenta e oito", t2);
       assertEquals("noventa", t3);
       assertEquals("cem", t4);
   }
   
   
   @Test
   public void testeMil() throws Exception
   {
       String t1 = classeTeste.formatarNumero(666).toLowerCase();
       String t2 = classeTeste.formatarNumero(404).toLowerCase();
       String t3 = classeTeste.formatarNumero(800).toLowerCase();
       String t4 = classeTeste.formatarNumero(999).toLowerCase();
       
       assertEquals("seiscentos e sessenta e seis", t1);
       assertEquals("quatrocentos e quatro", t2);
       assertEquals("oitocentos", t3);
       assertEquals("novecentos e noventa e nove", t4);
   }
   
   
   @Test
   public void testeMilhao() throws Exception
   {
       String t1 = classeTeste.formatarNumero(1000).toLowerCase();
       String t2 = classeTeste.formatarNumero(2030).toLowerCase();
       String t3 = classeTeste.formatarNumero(39098).toLowerCase();
       String t4 = classeTeste.formatarNumero(999999).toLowerCase();
       
       assertEquals("mil", t1);
       assertEquals("dois mil e trinta", t2);
       assertEquals("trinta e nove mil e noventa e oito", t3);
       assertEquals("novecentos e noventa e nove mil e novecentos e noventa e nove", t4);
   }
}
